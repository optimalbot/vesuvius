# Vesuvius

This is a simple CRUD project built for editing simple, normalized data fetched from an api. It is proudly built with Vue and hosted on bitbucket. 

## Project setup
```
npm install
```
### Start up test server on local machine
```
npm run serve
```
### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

Created by Omar Brown and is Open-Source
