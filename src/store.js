import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    'data':[],
     fields: [
      { key: 'name', label: 'Person Full name', sortable: true, sortDirection: 'desc' },
      { key: 'age', label: 'Person age', sortable: true, 'class': 'text-center' },
      {key:'address', label:'Address', sortable: true},
      { key: 'isActive', label: 'is Active' },
      { key: 'actions', label: 'Actions' }
    ]
    
  },
  mutations: {
    updateData (state, payload){
      state.data = payload.data
      }
  },
  actions: {
    fetchData(context){
        Axios.get('https://api.myjson.com/bins/1btvxc').then((response)=>{
        if(response){
          context.commit('updateData',response)
        }
        else{
          context.commit('updateData')        
        }       
      })
    }
  }
})
1